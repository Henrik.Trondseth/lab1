package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {

    public static void main(String[] args) {
        /*
         * The code here does two things:
         * It first creates a new RockPaperScissors -object with the
         * code `new RockPaperScissors()`. Then it calls the `run()`
         * method on the newly created object.
         */
        new RockPaperScissors().run();
    }

    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    Random random = new Random();
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");

    public void run() {
        while (true){
            System.out.println("Let's play round " + roundCounter);
            String conInp = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();
            int randomIndex = random.nextInt(rpsChoices.size());
            String compChoice = rpsChoices.get(randomIndex);
            if (!rpsChoices.contains(conInp)) {
                System.out.println("I do not understand " + conInp + ". Could you try again?");    
                continue;
            }
                if (conInp.equals(compChoice)){
                    System.out.println("Human chose " + conInp + ", computer chose " + compChoice + ". It's a tie!");
                    System.out.println("Score: human " + humanScore + ", computer " + computerScore);
                    String playOn = readInput("Do you wish to continue playing? (y/n)?").toLowerCase();
                    roundCounter++;
                    if (keepPlaying(playOn)) {
                        continue;
                    } else {
                        System.out.println("Bye bye :)");
                        break;
                    }
                } else {
                    if(checkResult(conInp, compChoice)){
                        System.out.println("Human chose " + conInp + ", computer chose " + compChoice + ". Human wins!");
                        humanScore++;
                        System.out.println("Score: human " + humanScore + ", computer " + computerScore);
                        String playOn = readInput("Do you wish to continue playing? (y/n)?").toLowerCase();
                        roundCounter++;
                        if (keepPlaying(playOn)) {
                            continue;
                        } else {
                            System.out.println("Bye bye :)");
                            break;
                        }
                    } else {
                        System.out.println("Human chose " + conInp + ", computer chose " + compChoice + ". Computer wins!");
                        computerScore++;
                        System.out.println("Score: human " + humanScore + ", computer " + computerScore);
                        String playOn = readInput("Do you wish to continue playing? (y/n)?").toLowerCase();
                        roundCounter++;
                        if (keepPlaying(playOn)) {
                            continue;
                        } else {
                            System.out.println("Bye bye :)");
                            break;
                        }
                    }
                }
            }
    }

    /**
     * Reads input from console with given prompt
     * 
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

    public boolean checkResult(String humanChoice, String choiceComp) {
        if (humanChoice.equals("rock") && choiceComp.equals("scissors")) {
            return true;
        } else if (humanChoice.equals("scissors") && choiceComp.equals("paper")) {
            return true;
        } else if (humanChoice.equals("paper") && choiceComp.equals("rock")) {
            return true;
        } else {
            return false;
        }
    }

    public boolean keepPlaying(String yesorno) {
        if (yesorno.equals("y")) {
            return true;
        } else {
            return false;
        }
    }
}